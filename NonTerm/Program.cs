﻿using System;
using System.IO;
using System.Diagnostics;
using Microsoft.Z3;
using System.Collections.Generic;

namespace NonTerm
{
	
	class MainClass
	{


		public static void proofPrint (List<string> model, int n, List<Tuple<string,string>> rls) {
			string s = "";
			string[] l;
			foreach (string q in model) {
				if (q [0] == 't') {
					l = q.Split ('_');
					s += string.Format ("{0} -{1}-> {2}\n", l[2], l[1], l[3]);
				}
			}
			s += "Non-termination proof is given by the automaton which satisfies conditions for non-termination of cycle rewriting. For more info please refer to https://drive.google.com/open?id=0B32iRhBlhFx7WFVOc2t5a0N5YlU\n";
			Console.WriteLine (s);
		}

		public static void fsmPrint (List<string> model, int n, List<Tuple<string,string>> rls) {
			string s = "";
			string[] l;
			s += "digraph sat_nfa {\n";
			s += "\tlabel=\"RULES: ";

			foreach (Tuple<string,string> r in rls) {
				s += r.Item1 + " -> " + r.Item2 + "; ";
			}

			s += "\";\n";

			s += "\tlabelloc=top;\n";
			s += "\tlabeljust=left;\n";
			s += "\trankdir = LR;\n";
			s += "\tnode [shape = point];si;\n";
			s += "\tnode [shape = circle, height = 0.5, fixedsize = true, fontsize = 14];\n";
			for (int i = 0; i < n; i++) {
				if (i == 1)
					s += "\ts_" + i + "[label = \"" + i + "\", shape = \"doublecircle\"];\n";
				else 
					s += "\ts_" + i + "[label = \"" + i + "\"];\n";
			}
			s += "\tsi -> s_0;\n";
			foreach (string q in model) {
				if (q [0] == 't') {
					l = q.Split ('_');
					s += "\ts_" + l[2] + " -> " + "s_" + l[3] + " [label = \"" + l[1] + "\"];\n";
				}
			}
			s += "}";

			using (StreamWriter writer =
				new StreamWriter("result.dot"))
			{
				writer.WriteLine(s);
			}

			Process.Start("dot", "-Tpdf result.dot -o result.pdf");

			Console.WriteLine (s);
		}

		public List<Tuple<string,string>> rlsDel (List<Tuple<string,string>> rls) {
			if (rls.Count > 1) {
				List<Tuple<string,string>> newRls = rls;
				newRls.RemoveAt (0);
				return newRls;
			} else
				return rls;
		}

		public List<Tuple<string,string>> rlsTransform (List<Tuple<string,string>> rls) {
			List<char> sig = Helpers.getSigma (rls);
			bool cond;
			string candidate;
			List<char> symbols = new List<char> ();
			List<Tuple<string,List<char>>> candidates = new List<Tuple<string, List<char>>> ();
			for (int i = 0; i < rls.Count; i++) {
				for (int j = 0; j < rls.Count; j++) {
					if (i != j) {
						if (rls [i].Item1 [rls [i].Item1.Length - 1] == rls [j].Item1 [0]) {
							symbols.Clear ();
							candidate = rls [i].Item1;
							foreach (char a in sig) {
								cond = true;
								for (int h = 0; h < rls.Count; h++) {
									if (h != i && h != j && rls [h].Item1.Length > 1) {
										if (rls [h].Item1 [1] != a)
											cond = false;
									}
								}
								if (cond)
									symbols.Add (a);
							}
							candidates.Add (Tuple.Create (candidate, symbols));
						}
					}
				}
			}
			return null;
		}

		public static void Main (string[] args)
		{
			
			List <Tuple<string,string>> rules = new List<Tuple<string, string>> { 

				
				Tuple.Create("0L","L1"),
				Tuple.Create("R1","0R"),
				Tuple.Create("1BL","B00R"),
				Tuple.Create("RB0","L11B"),
				

			};
			


			if (args.Length < 1) {
				Console.WriteLine ("Error: No input file provided\n");
			} else {
				Parser prsr = new Parser (args [0]);

				rules = prsr.rules;

				string formRules = Helpers.makeRules (rules);
				string arguments = String.Format ("-c \"echo '{0}' | ./nonnf\"", formRules);

				Stopwatch sw2 = new Stopwatch ();
				sw2.Start ();
				Process proc = new Process ();
				proc.StartInfo.FileName = "/bin/bash";
				proc.StartInfo.Arguments = arguments;
				proc.StartInfo.UseShellExecute = false;
				proc.StartInfo.RedirectStandardOutput = true;
				proc.Start ();

				string jout = "";
				while (!proc.StandardOutput.EndOfStream) {
					jout = proc.StandardOutput.ReadLine ();
				}

				jout = jout.Substring (1, jout.Length - 2);

				FSM<int> nf = Helpers.JSONToFSM (jout);
				sw2.Stop ();


				List<string> toPrint = new List<string> ();

				int i = 2;
				Stopwatch sw = new Stopwatch ();
				sw.Start ();
				while (true) {
					//Console.WriteLine ("Trying {0} states...", i);
					Prover p = new Prover (i, rules, nf);
					p.NonEmpty ();
					//p.ClosedUnderRewriting ();
					//p.ClosedunderRewritingAdv ();
					p.ClosedUnderRewrMethod2 ();
					//p.NoNormalForms ();
					p.NoNormalFormsAdv ();
					//p.ClosedUnderCyclic ();
					if (p.Solve ()) {
						Console.WriteLine ("NO");
						//Console.WriteLine ("SAT");
						sw.Stop ();
						foreach (FuncDecl d in p.model.Decls) {
							if (d.Name.ToString ().Substring (0, 1) == "t" || d.Name.ToString ().Substring (0, 1) == "f"
							   || d.Name.ToString ().Substring (0, 1) == "b" || d.Name.ToString ().Substring (0, 2) == "hh"
							   || d.Name.ToString ().Substring (0, 2) == "kk") {
								//Console.WriteLine (d.Name + " -> " + p.model.ConstInterp (d));
								if (p.model.ConstInterp (d).IsTrue)
									toPrint.Add (d.Name.ToString ());
							}
						}
						//Console.WriteLine ("Elapsed={0}", sw.Elapsed);
						//fsmPrint (toPrint, i, rules);
						proofPrint (toPrint, i, rules);

						break;
					} else {
						//Console.WriteLine ("UNSAT");
						i++;
					}
				}
			}


		}
	}
}
