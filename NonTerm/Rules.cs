﻿using System;
using System.Collections.Generic;

namespace NonTerm
{
	public class Rules
	{
		List<Rule> rules;

		public Rules (List<Rule> r)
		{
			rules = r;
		}

		public class Rule
		{
			public Rule(string l, string r)
			{
				Lhs = l;
				Rhs = r;
			}
			public string Lhs { get; set; }
			public string Rhs { get; set; }
		}
	}
}

