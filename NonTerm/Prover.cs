﻿using System;
using Microsoft.Z3;
using System.Collections.Generic;

namespace NonTerm
{
	public class Prover
	{
		private int stateBound;
		private Context ctx;
		private List<Tuple<string,string>> rules;
		public Model model;
		private FSM<int> nffsm;
		List<char> sigma = new List<char> ();
		private Dictionary<string,BoolExpr> formulas = new Dictionary<string, BoolExpr> ();
		private List<string> lhsides = new List<string> ();
		private List<string> rhsides = new List<string> ();

		public Prover (int bound, List<Tuple<string,string>> rls, FSM<int> n)
		{
			stateBound = bound;
			rules = rls;
			nffsm = n;
			sigma = Helpers.getSigma (rules);
			ctx = new Context ();
			string label, label1, label2, label3, label4, label5, label6;
			List<BoolExpr> bExpr = new List<BoolExpr> ();
			List<BoolExpr> bExpr1 = new List<BoolExpr> ();
			List<BoolExpr> bExpr2 = new List<BoolExpr> ();
			List<BoolExpr> bExpr3 = new List<BoolExpr> ();

			foreach (Tuple<string,string> r in rules) {
				lhsides.Add (r.Item1);
				rhsides.Add (r.Item2);
			}

			foreach (Tuple<string,string> r in rules) {
				if (r.Item1.Length > 1) {
					for (int i = 1; i < r.Item1.Length; i++) {
						if (!lhsides.Contains (r.Item1.Substring (0, i)))
							lhsides.Add (r.Item1.Substring (0, i));
						if (!lhsides.Contains (r.Item1.Substring (i, r.Item1.Length - i)))
							lhsides.Add (r.Item1.Substring (i, r.Item1.Length - i));
					}
				}

				if (r.Item2.Length > 1) {
					for (int i = 1; i < r.Item2.Length; i++) {
						if (!rhsides.Contains(r.Item2.Substring (0, i)))
							rhsides.Add (r.Item2.Substring (0, i));
						if (!rhsides.Contains(r.Item2.Substring (i, r.Item2.Length - i)))
							rhsides.Add (r.Item2.Substring (i, r.Item2.Length - i));
					}
				}
			}
				

			//create transition vars
			for (int i = 0; i < stateBound; i++) {
				for (int j = 0; j < stateBound; j++) {
					foreach (char a in sigma) {
						label1 = string.Format ("t_{0}_{1}_{2}", a, i, j);
						formulas.Add (label1, (BoolExpr)ctx.MkConst(label1, ctx.MkBoolSort()));
					}
				}
			}

			formulas.Add ("all", ctx.MkTrue());

			//MakeTest1 ();

			/*
			//create final states vars
			for (int i = 0; i < stateBound; i++) {
				label1 = string.Format ("f_{0}", i);
				formulas.Add (label1, (BoolExpr)ctx.MkConst (label1, ctx.MkBoolSort ()));
			}
			*/

			//MakeTest ();

			//at least one final state

			/*
			bExpr.Clear();
			for (int i = 1; i < stateBound; i++) {
				label1 = string.Format ("f_{0}", i);
				bExpr.Add (formulas [label1]);
			}
			formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkOr (bExpr.ToArray()));
			formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkNot(formulas ["f_0"]));
			*/

			//MakeTest ();

			//create reachability variables for all state pairs
			for (int k = 0; k < Math.Ceiling(Math.Log(stateBound, 2)); k++) {
				if (k == 0) {
					for (int i = 0; i < stateBound; i++) {
						for (int j = 0; j < stateBound; j++) {
							bExpr.Clear ();
							label1 = string.Format ("r_{0}_{1}_{2}",k,i,j);
							foreach (char a in nffsm.sigma) {
								label2 = string.Format ("t_{0}_{1}_{2}",a,i,j);
								bExpr.Add (formulas [label2]);
							}
							formulas.Add (label1, (BoolExpr)ctx.MkConst (label1, ctx.MkBoolSort()));
							formulas["all"] = ctx.MkAnd(formulas["all"], ctx.MkEq(formulas[label1], ctx.MkOr (bExpr.ToArray())));
							//formulas.Add (label1, ctx.MkOr (bExpr.ToArray()));
						}
					}
				} else {
					for (int i = 0; i < stateBound; i++) {
						for (int j = 0; j < stateBound; j++) {
							bExpr.Clear ();
							label1 = string.Format("r_{0}_{1}_{2}", k,i,j);
							for (int l = 0; l < stateBound; l++) {
								label2 = string.Format ("r_{0}_{1}_{2}", (k - 1), i, l);
								label3 = string.Format ("r_{0}_{1}_{2}", (k - 1), l, j);
								if (!formulas.ContainsKey (label2))
									formulas.Add (label2, (BoolExpr)ctx.MkConst (label2, ctx.MkBoolSort ()));
								if (!formulas.ContainsKey (label3))
									formulas.Add (label3, (BoolExpr)ctx.MkConst (label3, ctx.MkBoolSort ()));
								bExpr.Add (ctx.MkAnd (formulas [label2], formulas [label3]));
							}
							label4 = string.Format ("r_{0}_{1}_{2}", (k - 1), i, j);
							formulas.Add (label1, (BoolExpr)ctx.MkConst (label1, ctx.MkBoolSort ()));
							formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkEq (formulas[label1], ctx.MkOr (ctx.MkOr (bExpr.ToArray ()), formulas [label4])));
							//formulas.Add (label1, ctx.MkOr (ctx.MkOr (bExpr.ToArray ()), formulas [label4]));
						}
					}
				}
			}

			//create reachability by consuming a specific word for all state pairs
			foreach (string lhs in lhsides) {
				for (int h = 1; h <= lhs.Length; h++) {
					if (h == 1) {
						for (int i = 0; i < stateBound; i++) {
							for (int j = 0; j < stateBound; j++) {
								label1 = string.Format ("q_{0}_{1}_{2}", lhs.Substring (0, h),i,j);
								label2 = string.Format ("t_{0}_{1}_{2}", lhs.Substring (0, h),i,j);
								if (!formulas.ContainsKey (label1)) {
									formulas.Add (label1, (BoolExpr)ctx.MkConst (label1, ctx.MkBoolSort ()));
								}
								if (!formulas.ContainsKey (label2)) {
									formulas.Add (label2, (BoolExpr)ctx.MkConst (label2, ctx.MkBoolSort ()));
								}
								/*
								if (!formulas.ContainsKey (label1)) {
									formulas.Add (label1, formulas [label2]);
									formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkImplies ());
								}*/
								formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkEq(formulas[label1], formulas[label2]));
							}
						}
					} else {
						for (int i = 0; i < stateBound; i++) {
							for (int j = 0; j < stateBound; j++) {
								label1 = string.Format ("q_{0}_{1}_{2}", lhs.Substring (0, h), i, j);
								if (!formulas.ContainsKey (label1)) {
									formulas.Add (label1, (BoolExpr)ctx.MkConst (label1, ctx.MkBoolSort ()));
								}
								bExpr.Clear ();
								for (int l = 0; l <stateBound; l++) {
									label2 = string.Format ("q_{0}_{1}_{2}", lhs.Substring (0, h - 1), i, l);
									label3 = string.Format ("t_{0}_{1}_{2}", lhs.Substring (h - 1, 1), l, j);
									bExpr.Add (ctx.MkAnd (formulas[label2],formulas[label3]));
								}
								/*
								if (!formulas.ContainsKey(label1))
									formulas.Add (label1, ctx.MkOr(bExpr.ToArray()));
									*/
								formulas ["all"] = ctx.MkAnd (formulas["all"], ctx.MkEq(formulas[label1], ctx.MkOr(bExpr.ToArray())));
							}
						}
					}
				}
			}

			foreach (string rhs in rhsides) {
				for (int h = 1; h <= rhs.Length; h++) {
					if (h == 1) {
						for (int i = 0; i < stateBound; i++) {
							for (int j = 0; j < stateBound; j++) {
								label1 = string.Format ("q_{0}_{1}_{2}", rhs.Substring (0, h),i,j);
								label2 = string.Format ("t_{0}_{1}_{2}", rhs.Substring (0, h),i,j);
								if (!formulas.ContainsKey (label1)) {
									formulas.Add (label1, (BoolExpr)ctx.MkConst (label1, ctx.MkBoolSort ()));
								}
								if (!formulas.ContainsKey (label2)) {
									formulas.Add (label2, (BoolExpr)ctx.MkConst (label2, ctx.MkBoolSort ()));
								}
								/*if (!formulas.ContainsKey(label1) && formulas.ContainsKey(label2))
									formulas.Add (label1, formulas[label2]);*/
								formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkEq(formulas[label1], formulas[label2]));
							}
						}
					} else {
						for (int i = 0; i < stateBound; i++) {
							for (int j = 0; j < stateBound; j++) {
								label1 = string.Format ("q_{0}_{1}_{2}", rhs.Substring (0, h), i, j);
								if (!formulas.ContainsKey (label1)) {
									formulas.Add (label1, (BoolExpr)ctx.MkConst (label1, ctx.MkBoolSort ()));
								}
								bExpr.Clear ();
								for (int l = 0; l <stateBound; l++) {
									label2 = string.Format ("q_{0}_{1}_{2}", rhs.Substring (0, h - 1), i, l);
									label3 = string.Format ("t_{0}_{1}_{2}", rhs.Substring (h - 1, 1), l, j);
									//Console.WriteLine ("{0} {1}", label2, label3);
									bExpr.Add (ctx.MkAnd (formulas[label2],formulas[label3]));
								}
								/*
								if (!formulas.ContainsKey(label1))
									formulas.Add (label1, ctx.MkOr(bExpr.ToArray()));*/
								formulas ["all"] = ctx.MkAnd (formulas["all"], ctx.MkEq(formulas[label1], ctx.MkOr(bExpr.ToArray())));
							}
						}
					}
				}
			}

			//encode non nf automaton
			for (int i = 0; i < nffsm.states.Count; i++) {
				for (int j = 0; j < nffsm.states.Count; j++) {
					foreach (char a in nffsm.sigma) {
						label1 = string.Format ("n_{0}_{1}_{2}",a,i,j);
						if (nffsm.trans [Tuple.Create (i, a)] == j)
							formulas.Add (label1, ctx.MkTrue());
						else
							formulas.Add (label1, ctx.MkFalse());
					}
				}
			}

			for (int i = 0; i < nffsm.states.Count; i++) {
				label1 = string.Format ("nf_{0}", i);
				formulas.Add(label1, (BoolExpr)ctx.MkConst(label1, ctx.MkBoolSort()));
				if (!nffsm.finals.Contains (i))
					formulas [label1] = ctx.MkFalse ();
				else
					formulas [label1] = ctx.MkTrue ();
			}
			/*
			//encode product automaton transitions
			for (int i1 = 0; i1 < stateBound; i1++) {
				for (int i2 = 0; i2 < nffsm.states.Count; i2++) {
					for (int j1 = 0; j1 < stateBound; j1++) {
						for (int j2 = 0; j2 < nffsm.states.Count; j2++) {
							foreach (char a in nffsm.sigma) {
								label1 = string.Format ("e_{0}_{1}_{2}_{3}_{4}", a, i1, i2, j1, j2);
								label2 = string.Format ("t_{0}_{1}_{2}", a, i1, j1);
								label3 = string.Format ("n_{0}_{1}_{2}", a, i2, j2);
								formulas.Add (label1, ctx.MkAnd (formulas [label2], formulas [label3]));
							}
						}
					}
				}
			}

			//encode reachability in the product automaton
			for (int k = 0; k < Math.Ceiling (Math.Log ((stateBound * nffsm.states.Count), 2)); k++) {
				if (k == 0) {
					for (int i1 = 0; i1 < stateBound; i1++) {
						for (int i2 = 0; i2 < nffsm.states.Count; i2++) {
							for (int j1 = 0; j1 < stateBound; j1++) {
								for (int j2 = 0; j2 < nffsm.states.Count; j2++) {
									label1 = string.Format ("c_{0}_{1}_{2}_{3}_{4}",k,i1,i2,j1,j2);
									bExpr.Clear ();
									foreach (char a in nffsm.sigma) {
										label2 = string.Format ("e_{0}_{1}_{2}_{3}_{4}", a, i1, i2, j1, j2);
										bExpr.Add (formulas[label2]);
									}
									formulas.Add (label1, ctx.MkOr (bExpr.ToArray ()));
								}
							}
						}
					}
				} else {
					for (int i1 = 0; i1 < stateBound; i1++) {
						for (int i2 = 0; i2 < nffsm.states.Count; i2++) {
							for (int j1 = 0; j1 < stateBound; j1++) {
								for (int j2 = 0; j2 < nffsm.states.Count; j2++) {
									label1 = string.Format ("c_{0}_{1}_{2}_{3}_{4}",k,i1,i2,j1,j2);
									bExpr.Clear ();
									for (int g = 0; g < stateBound; g++) {
										for (int h = 0; h < nffsm.states.Count; h++) {
											label2 = string.Format ("c_{0}_{1}_{2}_{3}_{4}", k - 1, i1, i2, g, h);
											label3 = string.Format ("c_{0}_{1}_{2}_{3}_{4}", k - 1, g, h, j1, j2);
											bExpr.Add (ctx.MkAnd(formulas[label2],formulas[label3]));
										}
									}
									label4 = string.Format ("c_{0}_{1}_{2}_{3}_{4}", k - 1, i1, i2, j1, j2);
									formulas.Add (label1, ctx.MkOr (ctx.MkOr (bExpr.ToArray ()), formulas [label4]));
								}
							}
						}
					}
				}
			}
			*/	

			//cycle simulation variables
			foreach (char a in nffsm.sigma) {
				for (int i = 0; i < stateBound; i++) {
					for (int j = 0; j < stateBound; j++) {
						label1 = string.Format ("s_{0}_{1}_{2}", a, i, j);
						formulas.Add (label1, (BoolExpr)ctx.MkConst (label1, ctx.MkBoolSort ()));
					}
				}
			}


			/*
			//simulation formula
			foreach (char inp in nffsm.sigma) {
				for (int i = 0; i < stateBound; i++) {
					for (int j = 0; j < stateBound; j++) {
						bExpr3.Clear ();
						label1 = string.Format ("s_{0}_{1}_{2}", inp, i, j);
						foreach (char a in nffsm.sigma) {
							bExpr2.Clear ();
							for (int p = 0; p < stateBound; p++) {
								bExpr1.Clear ();
								for (int q = 0; q < stateBound; q++) {
									label2 = string.Format ("t_{0}_{1}_{2}", a, j, q);
									label3 = string.Format ("s_{0}_{1}_{2}", inp, p, q);
									bExpr1.Add (ctx.MkAnd (formulas [label2], formulas [label3]));
								}
								label2 = string.Format ("t_{0}_{1}_{2}", a, i, p);
								bExpr2.Add (ctx.MkImplies (formulas [label2], ctx.MkOr (bExpr1.ToArray ())));
							}
							bExpr3.Add (ctx.MkAnd (bExpr2.ToArray ()));
						}
						formulas["all"] = ctx.MkAnd(formulas["all"], ctx.MkImplies(formulas[label1], ctx.MkAnd (bExpr3.ToArray ())));
					}
				}
			}
			*/	


		}

		private void CycleSim(){
			string label, label1, label2, label3, label4, label5, label6;
			List<BoolExpr> bExpr = new List<BoolExpr> ();
			List<BoolExpr> bExpr1 = new List<BoolExpr> ();
			List<BoolExpr> bExpr2 = new List<BoolExpr> ();
			List<BoolExpr> bExpr3 = new List<BoolExpr> ();

			//simulation formula
			foreach (char inp in nffsm.sigma) {
				for (int i = 0; i < stateBound; i++) {
					for (int j = 0; j < stateBound; j++) {
						bExpr3.Clear ();
						label1 = string.Format ("s_{0}_{1}_{2}", inp, i, j);
						foreach (char a in nffsm.sigma) {
							bExpr2.Clear ();
							for (int p = 0; p < stateBound; p++) {
								bExpr1.Clear ();
								for (int q = 0; q < stateBound; q++) {
									label2 = string.Format ("t_{0}_{1}_{2}", a, j, q);
									label3 = string.Format ("s_{0}_{1}_{2}", inp, p, q);
									bExpr1.Add (ctx.MkAnd (formulas [label2], formulas [label3]));
								}
								label2 = string.Format ("t_{0}_{1}_{2}", a, i, p);
								bExpr2.Add (ctx.MkImplies (formulas [label2], ctx.MkOr (bExpr1.ToArray ())));
							}
							bExpr3.Add (ctx.MkAnd (bExpr2.ToArray ()));
						}
						formulas["all"] = ctx.MkAnd(formulas["all"], ctx.MkImplies(formulas[label1], ctx.MkAnd (bExpr3.ToArray ())));
					}
				}
			}
		}

		private void ForwardSim(){
			string label, label1, label2, label3, label4, label5, label6;
			List<BoolExpr> bExpr = new List<BoolExpr> ();
			List<BoolExpr> bExpr1 = new List<BoolExpr> ();
			List<BoolExpr> bExpr2 = new List<BoolExpr> ();
			List<BoolExpr> bExpr3 = new List<BoolExpr> ();

			for (int s = 0; s < stateBound; s++) {
				for (int i = 0; i < stateBound; i++) {
					for (int j = 0; j < stateBound; j++) {
						label1 = string.Format ("f{0}_{1}_{2}", s, i, j);
						formulas.Add (label1, (BoolExpr)ctx.MkConst (label1, ctx.MkBoolSort ()));
					}
				}
			}
				
			//simulation formula
			for (int s = 0; s < stateBound; s++) {
				for (int i = 0; i < stateBound; i++) {
					for (int j = 0; j < stateBound; j++) {
						bExpr3.Clear ();
						label1 = string.Format ("f{0}_{1}_{2}", s, i, j);
						foreach (char a in nffsm.sigma) {
							bExpr2.Clear ();
							for (int p = 0; p < stateBound; p++) {
								bExpr1.Clear ();
								for (int q = 0; q < stateBound; q++) {
									label2 = string.Format ("t_{0}_{1}_{2}", a, j, q);
									label3 = string.Format ("f{0}_{1}_{2}", s, p, q);
									bExpr1.Add (ctx.MkAnd (formulas [label2], formulas [label3]));
								}
								label2 = string.Format ("t_{0}_{1}_{2}", a, i, p);
								bExpr2.Add (ctx.MkImplies (formulas [label2], ctx.MkOr (bExpr1.ToArray ())));
							}
							bExpr3.Add (ctx.MkAnd (bExpr2.ToArray ()));
						}
						formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkImplies (formulas [label1], ctx.MkAnd (bExpr3.ToArray ())));
					}
				}
			}

			for (int i = 0; i < stateBound; i++) {
				for (int j = 0; j < stateBound; j++) {
					label1 = string.Format ("f{0}_1_{1}", i, j);
					if (i != j) {
						formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkNot (formulas [label1]));
					}
				}
			}



		}

		private void BackwardSim(){
			string label, label1, label2, label3, label4, label5, label6;
			List<BoolExpr> bExpr = new List<BoolExpr> ();
			List<BoolExpr> bExpr1 = new List<BoolExpr> ();
			List<BoolExpr> bExpr2 = new List<BoolExpr> ();
			List<BoolExpr> bExpr3 = new List<BoolExpr> ();

			for (int s = 0; s < stateBound; s++) {
				for (int i = 0; i < stateBound; i++) {
					for (int j = 0; j < stateBound; j++) {
						label1 = string.Format ("b{0}_{1}_{2}", s, i, j);
						formulas.Add (label1, (BoolExpr)ctx.MkConst (label1, ctx.MkBoolSort ()));
					}
				}
			}

			//simulation formula
			for (int s = 0; s < stateBound; s++) {
				for (int i = 0; i < stateBound; i++) {
					for (int j = 0; j < stateBound; j++) {
						bExpr3.Clear ();
						label1 = string.Format ("b{0}_{1}_{2}", s, i, j);
						foreach (char a in nffsm.sigma) {
							bExpr2.Clear ();
							for (int p = 0; p < stateBound; p++) {
								bExpr1.Clear ();
								for (int q = 0; q < stateBound; q++) {
									label2 = string.Format ("t_{0}_{1}_{2}", a, q, j);
									label3 = string.Format ("b{0}_{1}_{2}", s, p, q);
									bExpr1.Add (ctx.MkAnd (formulas [label2], formulas [label3]));
								}
								label2 = string.Format ("t_{0}_{1}_{2}", a, p, i);
								bExpr2.Add (ctx.MkImplies (formulas [label2], ctx.MkOr (bExpr1.ToArray ())));
							}
							bExpr3.Add (ctx.MkAnd (bExpr2.ToArray ()));
						}
						formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkImplies (formulas [label1], ctx.MkAnd (bExpr3.ToArray ())));
					}
				}
			}

			for (int i = 0; i < stateBound; i++) {
				for (int j = 0; j < stateBound; j++) {
					label1 = string.Format ("b{0}_0_{1}", i, j);
					if (i != j)
						formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkNot (formulas [label1]));
				}
			}

		}

		private void ForwardSim2 () {
			string label, label1, label2, label3, label4, label5, label6;
			List<BoolExpr> bExpr = new List<BoolExpr> ();
			List<BoolExpr> bExpr1 = new List<BoolExpr> ();
			List<BoolExpr> bExpr2 = new List<BoolExpr> ();
			List<BoolExpr> bExpr3 = new List<BoolExpr> ();

			for (int s = 0; s < stateBound; s++) {
				for (int i = 0; i < stateBound; i++) {
					for (int j = 0; j < stateBound; j++) {
						label1 = string.Format ("hh{0}_{1}_{2}", s, i, j);
						formulas.Add (label1, (BoolExpr)ctx.MkConst (label1, ctx.MkBoolSort ()));
					}
				}
			}

			//simulation formula
			for (int s = 0; s < stateBound; s++) {
				for (int i = 0; i < stateBound; i++) {
					for (int j = 0; j < stateBound; j++) {
						bExpr3.Clear ();
						label1 = string.Format ("hh{0}_{1}_{2}", s, i, j);
						foreach (char a in nffsm.sigma) {
							bExpr2.Clear ();
							for (int p = 0; p < stateBound; p++) {
								bExpr1.Clear ();
								for (int q = 0; q < stateBound; q++) {
									label2 = string.Format ("t_{0}_{1}_{2}", a, j, q);
									label3 = string.Format ("hh{0}_{1}_{2}", s, p, q);
									bExpr1.Add (ctx.MkAnd (formulas [label2], formulas [label3]));
								}
								label2 = string.Format ("t_{0}_{1}_{2}", a, i, p);
								bExpr2.Add (ctx.MkImplies (formulas [label2], ctx.MkOr (bExpr1.ToArray ())));
							}
							bExpr3.Add (ctx.MkAnd (bExpr2.ToArray ()));
						}
						formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkImplies (formulas [label1], ctx.MkAnd (bExpr3.ToArray ())));
					}
				}
			}

			for (int i = 0; i < stateBound; i++) {
				for (int j = 0; j < stateBound; j++) {
					label1 = string.Format ("hh{0}_{1}_{2}", i, i, j);
					if (j != 1) {
						formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkNot (formulas [label1]));
					}
				}
			}
		}

		private void BackwardSim2 () {
			string label, label1, label2, label3, label4, label5, label6;
			List<BoolExpr> bExpr = new List<BoolExpr> ();
			List<BoolExpr> bExpr1 = new List<BoolExpr> ();
			List<BoolExpr> bExpr2 = new List<BoolExpr> ();
			List<BoolExpr> bExpr3 = new List<BoolExpr> ();

			for (int s = 0; s < stateBound; s++) {
				for (int i = 0; i < stateBound; i++) {
					for (int j = 0; j < stateBound; j++) {
						label1 = string.Format ("kk{0}_{1}_{2}", s, i, j);
						formulas.Add (label1, (BoolExpr)ctx.MkConst (label1, ctx.MkBoolSort ()));
					}
				}
			}

			//simulation formula
			for (int s = 0; s < stateBound; s++) {
				for (int i = 0; i < stateBound; i++) {
					for (int j = 0; j < stateBound; j++) {
						bExpr3.Clear ();
						label1 = string.Format ("kk{0}_{1}_{2}", s, i, j);
						foreach (char a in nffsm.sigma) {
							bExpr2.Clear ();
							for (int p = 0; p < stateBound; p++) {
								bExpr1.Clear ();
								for (int q = 0; q < stateBound; q++) {
									label2 = string.Format ("t_{0}_{1}_{2}", a, q, j);
									label3 = string.Format ("kk{0}_{1}_{2}", s, p, q);
									bExpr1.Add (ctx.MkAnd (formulas [label2], formulas [label3]));
								}
								label2 = string.Format ("t_{0}_{1}_{2}", a, p, i);
								bExpr2.Add (ctx.MkImplies (formulas [label2], ctx.MkOr (bExpr1.ToArray ())));
							}
							bExpr3.Add (ctx.MkAnd (bExpr2.ToArray ()));
						}
						formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkImplies (formulas [label1], ctx.MkAnd (bExpr3.ToArray ())));
					}
				}
			}

			for (int i = 0; i < stateBound; i++) {
				for (int j = 0; j < stateBound; j++) {
					label1 = string.Format ("kk{0}_{1}_{2}", i, i, j);
					if (j != 0)
						formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkNot (formulas [label1]));
				}
			}
		}

		private void ForwardSimF(){
			string label, label1, label2, label3, label4, label5, label6;
			List<BoolExpr> bExpr = new List<BoolExpr> ();
			List<BoolExpr> bExpr1 = new List<BoolExpr> ();
			List<BoolExpr> bExpr2 = new List<BoolExpr> ();
			List<BoolExpr> bExpr3 = new List<BoolExpr> ();

			for (int i = 0; i < stateBound; i++) {
				for (int j = 0; j < stateBound; j++) {
					label1 = string.Format ("ff_{0}_{1}", i, j);
					formulas.Add (label1, (BoolExpr)ctx.MkConst (label1, ctx.MkBoolSort ()));
				}
			}

			//simulation formula
			for (int i = 0; i < stateBound; i++) {
				for (int j = 0; j < stateBound; j++) {
					bExpr3.Clear ();
					label1 = string.Format ("ff_{0}_{1}", i, j);
					foreach (char a in nffsm.sigma) {
						bExpr2.Clear ();
						for (int p = 0; p < stateBound; p++) {
							bExpr1.Clear ();
							for (int q = 0; q < stateBound; q++) {
								label2 = string.Format ("t_{0}_{1}_{2}", a, j, q);
								label3 = string.Format ("ff_{0}_{1}", p, q);
								bExpr1.Add (ctx.MkAnd (formulas [label2], formulas [label3]));
							}
							label2 = string.Format ("t_{0}_{1}_{2}", a, i, p);
							bExpr2.Add (ctx.MkImplies (formulas [label2], ctx.MkOr (bExpr1.ToArray ())));
						}
						bExpr3.Add (ctx.MkAnd (bExpr2.ToArray ()));
					}
					formulas["all"] = ctx.MkAnd(formulas["all"], ctx.MkImplies(formulas[label1], ctx.MkAnd (bExpr3.ToArray ())));
				}
			}

			for (int i = 0; i < stateBound; i++) {
				label1 = string.Format ("ff_1_{0}",i);
				if (i != 1) {
					formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkNot (formulas[label1]));
				}
			}

		}

		private void BackwardSimF(){
			string label, label1, label2, label3, label4, label5, label6;
			List<BoolExpr> bExpr = new List<BoolExpr> ();
			List<BoolExpr> bExpr1 = new List<BoolExpr> ();
			List<BoolExpr> bExpr2 = new List<BoolExpr> ();
			List<BoolExpr> bExpr3 = new List<BoolExpr> ();

			for (int i = 0; i < stateBound; i++) {
				for (int j = 0; j < stateBound; j++) {
					label1 = string.Format ("bf_{0}_{1}", i, j);
					formulas.Add (label1, (BoolExpr)ctx.MkConst (label1, ctx.MkBoolSort ()));
				}
			}

			//simulation formula
			for (int i = 0; i < stateBound; i++) {
				for (int j = 0; j < stateBound; j++) {
					bExpr3.Clear ();
					label1 = string.Format ("bf_{0}_{1}", i, j);
					foreach (char a in nffsm.sigma) {
						bExpr2.Clear ();
						for (int p = 0; p < stateBound; p++) {
							bExpr1.Clear ();
							for (int q = 0; q < stateBound; q++) {
								label2 = string.Format ("t_{0}_{1}_{2}", a, q, j);
								label3 = string.Format ("bf_{0}_{1}", p, q);
								bExpr1.Add (ctx.MkAnd (formulas [label2], formulas [label3]));
							}
							label2 = string.Format ("t_{0}_{1}_{2}", a, p, i);
							bExpr2.Add (ctx.MkImplies (formulas [label2], ctx.MkOr (bExpr1.ToArray ())));
						}
						bExpr3.Add (ctx.MkAnd (bExpr2.ToArray ()));
					}
					formulas["all"] = ctx.MkAnd(formulas["all"], ctx.MkImplies(formulas[label1], ctx.MkAnd (bExpr3.ToArray ())));
				}
			}

			for (int i = 0; i < stateBound; i++) {
				label1 = string.Format ("bf_0_{0}",i);
				if (i != 0) {
					formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkNot (formulas[label1]));
				}
			}

		}
			

		public void NonEmpty(){
			string label1, label2;

			/*
			for (int i = 1; i < stateBound; i++) {
				label1 = string.Format ("f_{0}",i);
				label2 = string.Format ("r_{0}_{1}_{2}", Math.Ceiling (Math.Log (stateBound, 2)) - 1, 0, i);
				formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkImplies (formulas [label1], formulas [label2]));
			}
			*/

			label1 = string.Format ("r_{0}_{1}_{2}", Math.Ceiling (Math.Log (stateBound, 2)) - 1, 0, 1);
			formulas ["all"] = ctx.MkAnd (formulas ["all"], formulas[label1]);

		}

		public void ClosedUnderRewriting(){
			string label1, label2;
			for (int i = 0; i < stateBound; i++) {
				for (int j = 0; j < stateBound; j++) {
					foreach (Tuple<string,string> r in rules) {
						label1 = string.Format ("q_{0}_{1}_{2}", r.Item1, i, j);
						label2 = string.Format ("q_{0}_{1}_{2}", r.Item2, i, j);
						formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkImplies (formulas[label1], formulas[label2]));
					}
				}
			}
		}

		public void ClosedunderRewritingAdv() {
			string label1, label2, label3, label4;
			List<BoolExpr> bExpr = new List<BoolExpr> ();
			List<BoolExpr> bExpr1 = new List<BoolExpr> ();
			List<BoolExpr> bExpr2 = new List<BoolExpr> ();
			List<BoolExpr> bExpr3 = new List<BoolExpr> ();

			//simulation with finals

			for (int i = 0; i < stateBound; i++) {
				for (int j = 0; j < stateBound; j++) {
					label1 = string.Format ("sim_{0}_{1}", i, j);
					formulas.Add (label1, (BoolExpr)ctx.MkConst (label1, ctx.MkBoolSort ()));
				}
			}
				

			//simulation formula with finals
			for (int i = 0; i < stateBound; i++) {
				for (int j = 0; j < stateBound; j++) {
					bExpr3.Clear ();
					label1 = string.Format ("sim_{0}_{1}", i, j);
					foreach (char a in nffsm.sigma) {
						bExpr2.Clear ();
						for (int p = 0; p < stateBound; p++) {
							bExpr1.Clear ();
							for (int q = 0; q < stateBound; q++) {
								label2 = string.Format ("t_{0}_{1}_{2}", a, j, q);
								label3 = string.Format ("sim_{0}_{1}", p, q);
								bExpr1.Add (ctx.MkAnd (formulas [label2], formulas [label3]));
							}
							label2 = string.Format ("t_{0}_{1}_{2}", a, i, p);
							bExpr2.Add (ctx.MkImplies (formulas [label2], ctx.MkOr (bExpr1.ToArray ())));
						}
						bExpr3.Add (ctx.MkAnd (bExpr2.ToArray ()));
					}
					formulas["all"] = ctx.MkAnd(formulas["all"], ctx.MkImplies(formulas[label1], ctx.MkAnd (bExpr3.ToArray ())));
				}
			}

			/*
			for (int i = 0; i < stateBound; i++) {
				for (int j = 0; j < stateBound; j++) {
					label1 = string.Format ("sim_{0}_{1}",i,j);
					label2 = string.Format ("f_{0}",i);
					label3 = string.Format ("f_{0}",j);
					formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkImplies(ctx.MkAnd(formulas[label1],formulas[label2]),formulas[label3]));
				}
			}
			*/

			for (int i = 0; i < stateBound; i++) {
				label1 = string.Format ("sim_1_{0}",i);
				if (i != 1) {
					formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkNot (formulas[label1]));
				}
			}

			for (int i = 0; i < stateBound; i++) {
				for (int j = 0; j < stateBound; j++) {
					foreach (Tuple<string,string> r in rules) {
						label1 = string.Format ("q_{0}_{1}_{2}", r.Item1, i, j);
						label2 = string.Format ("q_{0}_{1}_{2}", r.Item2, i, j);
						bExpr.Clear ();
						for (int k = 0; k < stateBound; k++) {
							label3 = string.Format ("q_{0}_{1}_{2}", r.Item2, i, k);
							label4 = string.Format ("sim_{0}_{1}", j, k);
							bExpr.Add (ctx.MkAnd(formulas[label3], formulas[label4]));
						}

						formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkOr(ctx.MkImplies (formulas[label1], formulas[label2]), 
							ctx.MkImplies(formulas[label1], ctx.MkOr(bExpr.ToArray()))));
					}
				}
			}

		}

		public void ClosedUnderRewrMethod2() {
			string label1, label2, label3, label4, label5, label6, label7, label8, label9, label10;
			List<BoolExpr> bExpr = new List<BoolExpr> ();
			List<BoolExpr> bExpr1 = new List<BoolExpr> ();
			List<BoolExpr> bExpr2 = new List<BoolExpr> ();
			List<BoolExpr> bExpr3 = new List<BoolExpr> ();

			ForwardSim ();
			BackwardSim ();
			ForwardSim2 ();
			BackwardSim2 ();
			ForwardSimF ();
			BackwardSimF ();

			for (int i = 0; i < stateBound; i++) {
				for (int j = 0; j < stateBound; j++) {
					foreach (Tuple<string,string> r in rules) {
						if (r.Item2.Length == 1) {
							label1 = string.Format ("q_{0}_{1}_{2}", r.Item1, i, j);
							bExpr1.Clear ();
							for (int k = 0; k < stateBound; k++) {
								for (int l = 0; l < stateBound; l++) {
									label2 = string.Format ("q_{0}_{1}_{2}", r.Item2, k, l);
									label3 = string.Format ("ff_{0}_{1}", j, l);
									label4 = string.Format ("bf_{0}_{1}", i, k);
									bExpr1.Add (ctx.MkAnd (formulas [label2], ctx.MkAnd (formulas [label3], formulas [label4])));
								}
							}
								
							formulas ["all"] = ctx.MkAnd(formulas["all"], ctx.MkImplies (formulas[label1], ctx.MkOr(bExpr1.ToArray())));
						} else {
							label1 = string.Format ("q_{0}_{1}_{2}", r.Item1, i, j);
							bExpr1.Clear ();
							for (int k = 0; k < stateBound; k++) {
								for (int l = 0; l < stateBound; l++) {
									label2 = string.Format ("q_{0}_{1}_{2}", r.Item2, k, l);
									label3 = string.Format ("ff_{0}_{1}", j, l);
									label4 = string.Format ("bf_{0}_{1}", i, k);
									//Console.WriteLine ("{0} : {1} $ {2} $ {3}", label1, label2, label3, label4);
									bExpr1.Add (ctx.MkAnd (formulas [label2], ctx.MkAnd (formulas [label3], formulas [label4])));
								}
							}

							bExpr2.Clear ();
							for (int h = 0; h < stateBound; h++) {
								for (int g = 0; g < stateBound; g++) {
									for (int t = 1; t < r.Item2.Length; t++) {
										label5 = string.Format ("q_{0}_0_{1}", r.Item2.Substring(t, r.Item2.Length - t), h);
										label6 = string.Format ("q_{0}_{1}_1", r.Item2.Substring (0, t), g);
										bExpr3.Clear ();
										for (int w = 0; w < stateBound; w++) {
											label9 = string.Format ("f{0}_{1}_{2}", w, j, h);
											label10 = string.Format ("b{0}_{1}_{2}", w, i, g);
											bExpr3.Add (ctx.MkAnd (formulas[label9], formulas[label10]));
										}
										//Console.WriteLine ("{0} : {1} $ {2} $ {3} $ {4}", label1, label5, label6, label7, label8);
										bExpr2.Add (ctx.MkAnd(formulas[label5], ctx.MkAnd(formulas[label6], ctx.MkOr(bExpr3.ToArray()))));
									}
								}
							}

							formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkImplies (formulas[label1], ctx.MkOr(ctx.MkOr(bExpr1.ToArray()), ctx.MkOr(bExpr2.ToArray()))));

						}
					}
				}
			}

			foreach (Tuple<string,string> r in rules) {
				if (r.Item2.Length > 1) {
					for (int i = 0; i < stateBound; i++) {
						for (int j = 0; j < stateBound; j++) {		
							for (int g = 1; g < r.Item1.Length; g++) {
								label1 = string.Format ("q_{0}_0_{1}", r.Item1.Substring (g, r.Item1.Length - g), i);
								label2 = string.Format ("q_{0}_{1}_1", r.Item1.Substring (0, g), j);
								bExpr1.Clear ();
								for (int h = 1; h < r.Item2.Length; h++) {
									label3 = string.Format ("q_{0}_0_{1}", r.Item2.Substring (h, r.Item2.Length - h), i);
									label4 = string.Format ("q_{0}_{1}_1", r.Item2.Substring (0, h), j);
									bExpr1.Add (ctx.MkAnd (formulas [label3], formulas [label4]));
								}

								bExpr2.Clear ();
								for (int d = 0; d < stateBound; d++) {
									label5 = string.Format ("q_{0}_0_{1}", r.Item2, d);
									label6 = string.Format ("q_{0}_{1}_1", r.Item2, d);
									label7 = string.Format ("hh{0}_{1}_{2}", j, i, d);
									label8 = string.Format ("kk{0}_{1}_{2}", i, j, d);
									bExpr2.Add (ctx.MkAnd (formulas [label5], formulas [label7]));
								}

								bExpr3.Clear ();
								for (int d = 0; d < stateBound; d++) {
									label5 = string.Format ("q_{0}_0_{1}", r.Item2, d);
									label6 = string.Format ("q_{0}_{1}_1", r.Item2, d);
									label7 = string.Format ("hh{0}_{1}_{2}", j, i, d);
									label8 = string.Format ("kk{0}_{1}_{2}", i, j, d);
									bExpr2.Add (ctx.MkAnd (formulas [label6], formulas [label8]));
								}

								formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkImplies (ctx.MkAnd (formulas [label1], formulas [label2]), ctx.MkOr (ctx.MkOr (bExpr1.ToArray ()), ctx.MkOr (ctx.MkOr (bExpr2.ToArray ()), ctx.MkOr (bExpr3.ToArray ())))));
							}
					
						}
					}
				} else {
					
					for (int i = 0; i < stateBound; i++) {
						for (int j = 0; j < stateBound; j++) {		
							for (int g = 1; g < r.Item1.Length; g++) {
								label1 = string.Format ("q_{0}_0_{1}", r.Item1.Substring (g, r.Item1.Length - g), i);
								label2 = string.Format ("q_{0}_{1}_1", r.Item1.Substring (0, g), j);

								bExpr2.Clear ();
								for (int d = 0; d < stateBound; d++) {
									label5 = string.Format ("q_{0}_0_{1}", r.Item2, d);
									label6 = string.Format ("q_{0}_{1}_1", r.Item2, d);
									label7 = string.Format ("hh{0}_{1}_{2}", j, i, d);
									label8 = string.Format ("kk{0}_{1}_{2}", i, j, d);
									bExpr2.Add (ctx.MkAnd (formulas [label5], formulas [label7]));
								}

								bExpr3.Clear ();
								for (int d = 0; d < stateBound; d++) {
									label5 = string.Format ("q_{0}_0_{1}", r.Item2, d);
									label6 = string.Format ("q_{0}_{1}_1", r.Item2, d);
									label7 = string.Format ("hh{0}_{1}_{2}", j, i, d);
									label8 = string.Format ("kk{0}_{1}_{2}", i, j, d);
									bExpr2.Add (ctx.MkAnd (formulas [label6], formulas [label8]));
								}

								formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkImplies (ctx.MkAnd (formulas [label1], formulas [label2]), ctx.MkOr (ctx.MkOr (ctx.MkOr (bExpr2.ToArray ()), ctx.MkOr (bExpr3.ToArray ())))));
							}
						}
					}
				}

			}
				

		}

		public void NoNormalForms() {
			string label1, label2, label3, label4;
			List<BoolExpr> bExpr = new List<BoolExpr> ();

			//encode product automaton transitions
			for (int i1 = 0; i1 < stateBound; i1++) {
				for (int i2 = 0; i2 < nffsm.states.Count; i2++) {
					for (int j1 = 0; j1 < stateBound; j1++) {
						for (int j2 = 0; j2 < nffsm.states.Count; j2++) {
							foreach (char a in nffsm.sigma) {
								label1 = string.Format ("e_{0}_{1}_{2}_{3}_{4}", a, i1, i2, j1, j2);
								label2 = string.Format ("t_{0}_{1}_{2}", a, i1, j1);
								label3 = string.Format ("n_{0}_{1}_{2}", a, i2, j2);
								formulas.Add (label1, ctx.MkAnd (formulas [label2], formulas [label3]));
							}
						}
					}
				}
			}

			//encode reachability in the product automaton
			for (int k = 0; k < Math.Ceiling (Math.Log ((stateBound * nffsm.states.Count), 2)); k++) {
				if (k == 0) {
					for (int i1 = 0; i1 < stateBound; i1++) {
						for (int i2 = 0; i2 < nffsm.states.Count; i2++) {
							for (int j1 = 0; j1 < stateBound; j1++) {
								for (int j2 = 0; j2 < nffsm.states.Count; j2++) {
									label1 = string.Format ("c_{0}_{1}_{2}_{3}_{4}",k,i1,i2,j1,j2);
									bExpr.Clear ();
									foreach (char a in nffsm.sigma) {
										label2 = string.Format ("e_{0}_{1}_{2}_{3}_{4}", a, i1, i2, j1, j2);
										bExpr.Add (formulas[label2]);
									}
									formulas.Add (label1, ctx.MkOr (bExpr.ToArray ()));
								}
							}
						}
					}
				} else {
					for (int i1 = 0; i1 < stateBound; i1++) {
						for (int i2 = 0; i2 < nffsm.states.Count; i2++) {
							for (int j1 = 0; j1 < stateBound; j1++) {
								for (int j2 = 0; j2 < nffsm.states.Count; j2++) {
									label1 = string.Format ("c_{0}_{1}_{2}_{3}_{4}",k,i1,i2,j1,j2);
									bExpr.Clear ();
									for (int g = 0; g < stateBound; g++) {
										for (int h = 0; h < nffsm.states.Count; h++) {
											label2 = string.Format ("c_{0}_{1}_{2}_{3}_{4}", k - 1, i1, i2, g, h);
											label3 = string.Format ("c_{0}_{1}_{2}_{3}_{4}", k - 1, g, h, j1, j2);
											bExpr.Add (ctx.MkAnd(formulas[label2],formulas[label3]));
										}
									}
									label4 = string.Format ("c_{0}_{1}_{2}_{3}_{4}", k - 1, i1, i2, j1, j2);
									formulas.Add (label1, ctx.MkOr (ctx.MkOr (bExpr.ToArray ()), formulas [label4]));
								}
							}
						}
					}
				}
			}

			/*
			for (int i = 1; i < stateBound; i++) {
				label = string.Format ("f_{0}", i);
				for (int p = 0; p < nffsm.states.Count; p++) {
					if (!nffsm.finals.Contains (p)) {
						label1 = string.Format ("c_{0}_0_0_{1}_{2}", Math.Ceiling (Math.Log ((stateBound * nffsm.states.Count), 2)) - 1, i, p);
						formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkImplies(formulas[label], ctx.MkNot (formulas [label1])));
					}
				}
			}*/

			for (int p = 0; p < nffsm.states.Count; p++) {
				if (!nffsm.finals.Contains (p)) {
					label1 = string.Format ("c_{0}_0_0_1_{1}", Math.Ceiling (Math.Log ((stateBound * nffsm.states.Count), 2)) - 1, p);
					formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkNot (formulas [label1]));
				}
			}
				
		}

		public void NoNormalFormsAdv() {
			string label, label1, label2, label3, label4;
			List<BoolExpr> bExpr = new List<BoolExpr> ();
			List<BoolExpr> bExpr2 = new List<BoolExpr> ();
			List<BoolExpr> bExpr3 = new List<BoolExpr> ();

			for (int i = 0; i < stateBound; i++) {
				for (int j = 0; j < nffsm.states.Count; j++) {
					label = string.Format ("sp_{0}_{1}", i, j);
					formulas.Add (label, (BoolExpr)ctx.MkConst (label, ctx.MkBoolSort ()));
				}
			}

			for (int i = 0; i < stateBound; i++) {
				for (int j = 0; j < nffsm.states.Count; j++) {
					label1 = string.Format ("sp_{0}_{1}", i, j);
					bExpr.Clear ();
					foreach (char a in nffsm.sigma) {
						bExpr3.Clear ();
						for (int p = 0; p < stateBound; p++) {
							label2 = string.Format ("t_{0}_{1}_{2}", a, i, p);
							bExpr2.Clear ();
							for (int q = 0; q < nffsm.states.Count; q++) {
								label3 = string.Format ("n_{0}_{1}_{2}", a, j, q);
								label4 = string.Format ("sp_{0}_{1}", p, q);
								bExpr2.Add (ctx.MkAnd(formulas[label3], formulas[label4]));
							}
							bExpr3.Add (ctx.MkImplies (formulas[label2], ctx.MkOr(bExpr2.ToArray())));
						}
						bExpr.Add (ctx.MkAnd (bExpr3.ToArray ()));
					}
					formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkImplies(formulas[label1], ctx.MkAnd (bExpr.ToArray ())));
				}
			}
				
			for (int j = 0; j < nffsm.states.Count; j++) {
				label1 = string.Format ("sp_1_{0}",j);
				label3 = string.Format ("nf_{0}",j);
				formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkImplies(formulas[label1],formulas[label3]));
			}
			label = string.Format ("sp_0_0");
			formulas ["all"] = ctx.MkAnd (formulas["all"], formulas[label]);


		}

		/*
		public void ClosedUnderCyclic () {
			string label, label1, label2, label3, label4;

			ForwardSim ();

			foreach (char a in nffsm.sigma) {
				for (int i = 0; i < stateBound; i++) {
					for (int j = 0; j < stateBound; j++) {
						label1 = string.Format ("t_{0}_0_{1}", a, i);
						label2 = string.Format ("s_{0}_{1}_0", a, i);
						formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkImplies (formulas [label1], formulas [label2]));

						for (int f = 1; f < stateBound; f++) {
							label3 = string.Format ("t_{0}_{1}_{2}", a, j, f);
							label4 = string.Format ("s_{0}_{1}_{2}", a, f, j);
							label = string.Format ("f_{0}",f);
							formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkImplies(formulas[label],ctx.MkImplies (formulas [label4], formulas [label3])));
						}
					}
				}
			}
		}
		*/


		public void MakeTest () {
			List<char> alph = new List<char>() { 'a', 'b', 'c' };
			foreach (char a in alph) {
				for (int i = 0; i < 12; i++) {
					for (int j = 0; j < 12; j++) {
						formulas ["t_" + a + "_" + i + "_" + j] = ctx.MkFalse ();
					}
				}
			}
			formulas ["t_a_0_2"] = ctx.MkTrue ();
			formulas ["t_b_2_3"] = ctx.MkTrue ();
			formulas ["t_c_3_4"] = ctx.MkTrue ();
			formulas ["t_b_4_1"] = ctx.MkTrue ();
			formulas ["t_c_0_5"] = ctx.MkTrue ();
			formulas ["t_b_5_6"] = ctx.MkTrue ();
			formulas ["t_a_6_4"] = ctx.MkTrue ();
			formulas ["t_b_0_7"] = ctx.MkTrue ();
			formulas ["t_c_7_8"] = ctx.MkTrue ();
			formulas ["t_b_8_9"] = ctx.MkTrue ();
			formulas ["t_a_9_1"] = ctx.MkTrue ();
			formulas ["t_a_7_10"] = ctx.MkTrue ();
			formulas ["t_b_10_11"] = ctx.MkTrue ();
			formulas ["t_c_11_1"] = ctx.MkTrue ();
			formulas ["f_1"] = ctx.MkTrue ();
			formulas ["f_0"] = ctx.MkFalse ();
			formulas ["f_2"] = ctx.MkFalse ();
			formulas ["f_3"] = ctx.MkFalse ();
			formulas ["f_4"] = ctx.MkFalse ();
			formulas ["f_5"] = ctx.MkFalse ();
			formulas ["f_6"] = ctx.MkFalse ();
			formulas ["f_7"] = ctx.MkFalse ();
			formulas ["f_8"] = ctx.MkFalse ();
			formulas ["f_9"] = ctx.MkFalse ();
			formulas ["f_10"] = ctx.MkFalse ();
			formulas ["f_11"] = ctx.MkFalse ();
		}

		public void MakeTest1 () {
			List<char> alph = new List<char>() { 'a', 'b', 'c' };
			foreach (char a in alph) {
				for (int i = 0; i < 6; i++) {
					for (int j = 0; j < 6; j++) {
						formulas ["t_" + a + "_" + i + "_" + j] = ctx.MkFalse ();
					}
				}
			}

			formulas ["t_a_0_2"] = ctx.MkTrue ();
			formulas ["t_b_2_3"] = ctx.MkTrue ();
			formulas ["t_c_3_4"] = ctx.MkTrue ();
			formulas ["t_b_4_1"] = ctx.MkTrue ();

		}

		public void ClosedUnderCyclic () {
			string label1, label2, label3, label4;

			CycleSim ();

			foreach (char a in nffsm.sigma) {
				for (int i = 0; i < stateBound; i++) {
					for (int j = 0; j < stateBound; j++) {
						label1 = string.Format ("t_{0}_0_{1}", a, i);
						label2 = string.Format ("s_{0}_{1}_0", a, i);
						label3 = string.Format ("t_{0}_{1}_1", a, j);
						label4 = string.Format ("s_{0}_1_{1}", a, j);
						formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkImplies (formulas [label1], formulas [label2]));
						formulas ["all"] = ctx.MkAnd (formulas ["all"], ctx.MkImplies (formulas [label4], formulas [label3]));
					}
				}
			}
		}
			

		public bool Solve() {
			Solver s = ctx.MkSolver ();
			s.Assert (formulas ["all"]);
			if (s.Check () == Status.SATISFIABLE) {
				model = s.Model;
				return true;
			}
			else return false;
		}

	}
}

