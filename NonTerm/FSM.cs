﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Z3;

namespace NonTerm
{

	public class Helpers
	{

		public static string makeRules (List<Tuple<string,string>> rls) {
			string res = "(RULES ";
			int j = 0;
			foreach (Tuple<string,string> r in rls) {
				for (int i = 0; i < r.Item1.Length; i++) {
					res += r.Item1 [i];
					res += " ";
				}
				res += "-> ";
				for (int i = 0; i < r.Item2.Length; i++) {
					res += r.Item2 [i];
					if (i < r.Item2.Length - 1) res += " ";
				}
				j++;
				if (j < rls.Count) {
					res += ",";	
				}
			}
			res += ")";
			return res;
		}

		public static List<char> getSigma(List<Tuple<string,string>> rls){
			List<char> res = new List<char> ();

			foreach (Tuple<string,string> r in rls) {
				char[] tmpSymbols = r.Item1.ToCharArray ();
				char[] tmpSymbols2 = r.Item2.ToCharArray ();
				res = res.Union (tmpSymbols).ToList();
				res = res.Union (tmpSymbols2).ToList ();
			}

			return res;
		}

		public static List<string> getNonNF(List<Tuple<string,string>> rls) {
			List<string> res = new List<string> ();

			foreach (Tuple<string,string> r in rls) {
				res.Add (r.Item1);
			}

			return res;

		}

		public static List<string> permutate (string s) {
			List<string> res = new List<string> ();
			if (s.Length == 1) {
				res.Add (s);
				return res;
			}
			string tmpStr = s + '$';
			for (int i = 1; i < s.Length; i++) {
				tmpStr = tmpStr.Substring (1, tmpStr.Length - 1) + tmpStr.Substring (0, 1);
				res.Add (tmpStr);
			}

			res = res.Distinct ().ToList ();

			return res;
		}

		public static List<string> getShiftedNonNF(List<Tuple<string,string>> rls) {
			List<string> res = new List<string> ();
			List<string> l = getNonNF (rls);

			foreach (string lhs in l) {
				res = res.Union (permutate (lhs)).ToList();
			}

			return res;
		}

		public static List<string> getA (List<Tuple<string,string>> rls) {
			List<string> res = new List<string> ();
			List<string> l = getNonNF (rls);

			foreach (string lhs in l) {
				for (int i = 0; i <= lhs.Length; i++) {
					res.Add (lhs.Substring (0, i));
				}
			}

			res = res.Distinct ().ToList ();

			return res;
		}

		public static List<string> getB (List<Tuple<string,string>> rls) {
			List<string> res = new List<string> ();
			List<string> l = getShiftedNonNF (rls);

			foreach (string h in l) {
				for (int i = 0; i <= h.Length; i++) {
					if (i < h.Length) {
						if (h [i] != '$')
							res.Add (h.Substring (0, i));
					}
					else
						res.Add (h.Substring (0, i));
				}
			}

			//res = res.Where (x => !res.Contains (x + '$')).ToList();
			res.Add ("@");
			res = res.Distinct ().ToList ();

			return res;
		}

		public static string matchA (string s, List<Tuple<string,string>> rls) {
			List<string> l = getNonNF (rls);
			List<string> setA = getA (rls);

			if (l.Contains (s.Substring (0, s.Length - 1))) {
				return s.Substring (0, s.Length - 1);
			} else if (setA.Contains (s)) {
				return s;
			} else if (s.Length > 1) {
				return matchA (s.Substring (1, s.Length - 1), rls);
			} else
				return "";
				
		}

		public static string matchB (string s, string lb) {
			List<string> setB = new List<string> ();

			for (int i = 0; i <= lb.Length; i++) {
				if (i < lb.Length) {
					if (lb [i] != '$')
						setB.Add (lb.Substring (0, i));
				}
				else
					setB.Add (lb.Substring (0, i));
			}
			setB.Add ("@");

			if (setB.Contains (s)) {
				return s;
			} else if (setB.Contains (s + '$')) {
				return s + '$';
			} else if (s.Contains ('$')) {
				string[] split = s.Split ('$');
				return matchB (split [0] + '$' + split [1].Substring (1, split [1].Length - 1),lb);
			} else return "@";

		}

		public static FSM<int> makeA (List<Tuple<string,string>> rls) {
			List<string> setA = getA (rls);
			List<string> nonnf = getNonNF (rls);

			Dictionary<string, int> stateMap = new Dictionary<string, int> ();

			int k = 0;
			List<int> q = new List<int> ();
			List<int> f = new List<int> ();
			foreach (string l in setA) {
				stateMap.Add (l, k);
				q.Add (k);
				k++;
				if (nonnf.Contains (l))
					f.Add (stateMap [l]);
			}

			List<char> sig = getSigma (rls);
			/*
			foreach (KeyValuePair<string,int> kvp in stateMap) {
				Console.WriteLine ("{0} - {1}", kvp.Key, kvp.Value);
			}
			*/
			Dictionary<Tuple<int,char>,int> t = new Dictionary<Tuple<int, char>, int> ();

			foreach (string l in setA) {
				foreach (char a in sig) {
					t.Add(Tuple.Create(stateMap[l],a),stateMap[matchA(l+a,rls)]);
				}
			}
				
	
			return new FSM<int>(q,sig,t,0,f);
		}

		public static FSM<int> makeB (string lb, List<Tuple<string,string>> rls) {
			List<string> setB = new List<string> ();

			for (int i = 0; i <= lb.Length; i++) {
				if (i < lb.Length) {
					if (lb [i] != '$')
						setB.Add (lb.Substring (0, i));
				}
				else
					setB.Add (lb.Substring (0, i));
			}
			setB.Add ("@");

			foreach (string s in setB)
				Console.WriteLine (s);
			string nonnfsh = lb;

			Dictionary<string, int> stateMap = new Dictionary<string, int> ();

			int k = 0;
			List<int> q = new List<int> ();
			List<int> f = new List<int> ();
			foreach (string l in setB) {
				stateMap.Add (l, k);
				q.Add (k);
				k++;
				if (nonnfsh == l)
					f.Add (stateMap [l]);
			}
			/*
			foreach (KeyValuePair<string,int> kvp in stateMap) {
				Console.WriteLine ("{0} - {1}", kvp.Key, kvp.Value);
			}
			*/

			List<char> sig = getSigma (rls);

			Dictionary<Tuple<int,char>,int> t = new Dictionary<Tuple<int, char>, int> ();

			foreach (string l in setB) {
				foreach (char a in sig) {
					t.Add(Tuple.Create(stateMap[l],a),stateMap[matchB(l+a,lb)]);
				}
			}


			return new FSM<int>(q,sig,t,0,f);
		}

		public static FSM<int> makeNonNF (List<Tuple<string,string>> rls) {
			FSM<int> mA = makeA (rls);
			//Console.WriteLine ("A is made");
			FSM<int> mB;
			ProductFSM<int> mP;
			List<string> lShifted = getShiftedNonNF (rls);

			foreach (string l in lShifted) {
				mB = makeB (l,rls);
				//Console.WriteLine ("B is made");
				//mB = mB.Integerize ();
				//mB.minimize ();
				//mB.removeUnreach ();
				mP = new ProductFSM<int> (mA, mB);
				Console.WriteLine ("Product is computed");
				mA = mP.Integerize ();
				Console.WriteLine ("Integerized");
				mA.minimize();
				Console.WriteLine ("Minimized");
				mA.removeUnreach ();
				Console.WriteLine ("Unreachables removed");
				mA = mA.Integerize ();
				Console.WriteLine ("Integerized 2");
			}

			//FSM<int> mB = makeB ("00$1", rls);

			//List<string> nonnf = getNonNF (rls);

			/*
			ProductFSM<int> mP = new ProductFSM<int> (mA, mB);
			FSM<int> res = mP.Integerize ();
			res.minimize();
			res.removeUnreach ();
			res = res.Integerize ();
			*/

			return mA;
		}

		public static FSM<int> JSONToFSM (string j) {
			string toparse, rest;
			List<int> states = new List<int> ();
			List<char> sigma = new List<char> ();
			Dictionary<Tuple<int,char>,int> trans = new Dictionary<Tuple<int, char>, int> ();
			int init = 0;
			List<int> finals = new List<int> ();
			//get rid of curly brackets
			if (j [0] != '{' || j [j.Length - 1] != '}') {
				Console.WriteLine (j);
				throw new Exception ("Wrong data");
			} else {
				rest = j.Substring (1, j.Length - 2);
			}
				
			//here we go through the string, highlight the needed elements, in case can not highlight a needen element - we through an exeption

			//highlight a states part, the rest of the strings goes to the rest
			toparse = "";
			while (rest [0] != ']') {
				toparse += rest [0];
				rest = rest.Substring (1,rest.Length-1);
			}
			toparse += rest [0];
			rest = rest.Substring (1,rest.Length-1);

			states = parseStates (toparse);

			if (rest [0] == ',') {
				rest = rest.Substring (1);
			} else {
				throw new Exception ("Wrong data");
			}

			toparse = "";
			while (rest [0] != ',') {
				toparse += rest [0];
				rest = rest.Substring (1);
			}

			sigma = parseSigma (toparse);

			if (rest [0] == ',') {
				rest = rest.Substring (1);
			} else {
				throw new Exception ("Wrong data");
			}

			toparse = "";

			while (rest.Length > 1 && rest.Substring(0,2) != "]]") {
				toparse += rest [0];
				rest = rest.Substring (1);
			}

			if (rest.Length > 1) {
				toparse += rest [0];
				toparse += rest [1];
				rest = rest.Substring (2);
			} else {
				throw new Exception ("Wrong data");
			}
				
			trans = parseTrans (toparse);

			if (rest [0] == ',') {
				rest = rest.Substring (1);
			} else {
				throw new Exception ("Wrong data");
			}

			if (rest.Length > 10) {
				rest = rest.Substring (10);
			} else {
				throw new Exception ("Wrong data");
			}
				
			if (rest [0] == ',') {
				rest = rest.Substring (1);
			} else {
				throw new Exception ("Wrong data");
			}
			if (rest.Length > 11) {
				rest = rest.Substring (11);
			}
				
			if (rest.Length > 2) {
				rest = rest.Substring (1, rest.Length - 2);
			} else {
				throw new Exception ("Wrong data");
			}
				
			finals = rest.Split (',').Select(int.Parse).ToList();

			return new FSM<int>(states,sigma,trans,init,finals);
		}

		private static List<int> parseStates (string j) {
			//gets a string, returns a parsed list of states
			string parsing = j;
			List<int> res = new List<int> ();
			if (parsing.Length > 11) {
				if (parsing.Substring (0, 11) == "\\\"states\\\":") {
					parsing = parsing.Substring (11);
				} else {
					throw new Exception ("Wrong data");
				}
			} else {
				throw new Exception ("Wrong data");
			}
				
			if (parsing [0] == '[' && parsing [parsing.Length - 1] == ']') {
				parsing = parsing.Substring (1, parsing.Length - 2);
			} else {
				throw new Exception ("Wrong data");
			}

			res = parsing.Split (',').Select(int.Parse).ToList();

			return res;
		}

		private static List<char> parseSigma (string j) {
			string parsing = j;
			List<char> res = new List<char> ();
			if (parsing.Length > 10) {
				if (parsing.Substring (0, 10) == "\\\"sigma\\\":") {
					parsing = parsing.Substring (10);
				} else {
					throw new Exception ("Wrong data");
				}
			} else {
				throw new Exception ("Wrong data");
			}

			if (parsing.Length > 4) {
				parsing = parsing.Substring (2, parsing.Length - 4);
			} else {
				throw new Exception ("Wrong data");
			}

			res = parsing.ToCharArray ().ToList ();
			return res;
		}

		private static Dictionary<Tuple<int,char>,int> parseTrans (string j) {
			string parsing = j;
			Dictionary<Tuple<int,char>,int> res = new Dictionary<Tuple<int, char>, int> ();
			Tuple<Tuple<int,char>,int> tmpTup;
			string toparse;
			if (parsing.Length > 10) {
				if (parsing.Substring (0, 10) == "\\\"delta\\\":") {
					parsing = parsing.Substring (10);
				} else {
					throw new Exception ("Wrong data");
				}
			} else {
				throw new Exception ("Wrong data");
			}

			if (parsing [0] == '[' && parsing [parsing.Length - 1] == ']') {
				parsing = parsing.Substring (1, parsing.Length - 2);
			} else {
				throw new Exception ("Wrong data");
			}

			while (parsing.Count() > 0) {
				if (parsing [0] == ',') {
					parsing = parsing.Substring (1);
				}
				toparse = "";
				if (parsing.Count() > 0) {
					while (parsing [0] != ']') {
						toparse += parsing [0];
						parsing = parsing.Substring (1);
					}
					toparse += parsing [0];
					parsing = parsing.Substring (1);
					tmpTup = parseOneTrans (toparse);
					res.Add (tmpTup.Item1, tmpTup.Item2);
				}
			}

			return res;
		}

		private static Tuple<Tuple<int,char>, int> parseOneTrans (string j) {
			string parsing = j;
			List<string> tmpList = new List<string> ();
			Tuple<Tuple<int,char>,int> res;
			if (parsing.Length > 3) {
				parsing = parsing.Substring (1, parsing.Length - 2);
			} else {
				throw new Exception ("Wrong data");
			}
				
			tmpList = parsing.Split (',').ToList ();
			if (tmpList.Count == 3) {
				res = Tuple.Create<Tuple<int,char>,int> (Tuple.Create<int,char> (int.Parse (tmpList [0]), tmpList [1] [2]), int.Parse (tmpList [2]));
			} else {
				throw new Exception ("Wrong data");
			}

			return res;
		}

	}

	public class JSONFSM
	{
		public List<int> states;
		public string sigma;
		public List<Tuple<int,char,int>> delta;
		public List<int> init;
		public List<int> finals;
	}


	public class FSM<T> 
	{
		public List<T> states;
		public List<char> sigma;
		public Dictionary<Tuple<T,char>,T> trans;
		public T initial;
		public List<T> finals;

		private Dictionary<T, int> stateMap = new Dictionary<T, int> ();
		private List<Tuple<Tuple<T,T>,bool>> statePairs = new List<Tuple<Tuple<T, T>,bool>> ();

		protected FSM ()
		{
		}

		public FSM (List<T> s, List<char> sig, Dictionary<Tuple<T,char>,T> trans, T init, List<T> fin) 
		{
			this.states = s;
			this.sigma = sig;
			this.trans = trans;
			this.initial = init;
			this.finals = fin;

			int i = 0;

			foreach (T x in this.states) {
				stateMap.Add (x, i);
				i++;
			}
				

			for (int q = 0; q < s.Count; q++) {
				for (int p = q + 1; p < s.Count; p++) {
					if (finals.Contains (s[q]) && !finals.Contains (s[p]) || !finals.Contains (s[q]) && finals.Contains (s[p]))
						statePairs.Add (Tuple.Create (Tuple.Create (s[q], s[p]), true));
					else
						statePairs.Add (Tuple.Create (Tuple.Create (s[q], s[p]), false));					
				}
			}

		}

		public void Print ()
		{
			Console.WriteLine ("states: ");
			foreach (T s in states) {
				Console.Write ("{0} ", s);
			}
			Console.WriteLine ("\nalphabet:");
			foreach (char a in sigma) {
				Console.Write ("{0} ", a);
			}
			Console.WriteLine ("\ntransitions:");
			foreach (KeyValuePair<Tuple<T,char>,T> t in trans) {
				Console.WriteLine ("{0} -{1}-> {2}", t.Key.Item1, t.Key.Item2, t.Value);
			}
			Console.WriteLine ("initial: \n{0}", initial);
			Console.WriteLine ("finals: ");
			foreach (T f in finals) {
				Console.Write ("{0} ", f);
			}
			Console.WriteLine ("");
		}

		public FSM<int> Integerize () 
		{
			stateMap = new Dictionary<T, int> ();

			int i = 0;

			foreach (T x in this.states) {
				stateMap.Add (x, i);
				i++;
			}

			List<int> statesi = new List<int> ();
			foreach (T s in this.states) {
				if (stateMap.ContainsKey (s))
					statesi.Add (stateMap [s]);
				else
					throw new Exception ("Can not integerize the FSM");
			}

			Dictionary<Tuple<int,char>,int> transi = new Dictionary<Tuple<int, char>, int> ();
			foreach (KeyValuePair<Tuple<T,char>,T> t in this.trans) {
				if (stateMap.ContainsKey (t.Key.Item1) && stateMap.ContainsKey (t.Value))
					transi.Add (Tuple.Create (stateMap [t.Key.Item1], t.Key.Item2), stateMap [t.Value]);
				else
					throw new Exception ("Can not integerize the FSM");
			}

			int initi = 0;
			if (stateMap.ContainsKey (this.initial))
				initi = stateMap [this.initial];
			else
				throw new Exception ("Can not integerize the FSM");

			List<int> fini = new List<int> ();
			foreach (T f in this.finals) {
				if (stateMap.ContainsKey (f))
					fini.Add (stateMap [f]);
				else
					throw new Exception ("Can not integerize the FSM");
			}
				
			return new FSM<int> (statesi, this.sigma, transi, initi, fini);
		}

		public bool isReachable (T state)
		{
			return isReachable (initial, state, new List<T> ());
		}

		private bool isReachable (T state, T target, List<T> visited)
		{
			List<T> succ = new List<T> ();
			List<T> newVisited = new List<T> ();
			bool res = false;
			if (visited.Contains (target) || state.Equals(target))
				return true;
			else if (visited.Count == this.states.Count)
				return false;
			else {
				foreach (char a in this.sigma) {
					if (trans.ContainsKey (Tuple.Create (state, a)))
						succ.Add (trans [Tuple.Create (state, a)]);
				}
				newVisited = visited.Union (succ).ToList();
				if (newVisited.Count == visited.Count)
					return false;
				foreach (T s in succ) {
					res = res || isReachable (s, target, newVisited);
				}
			}
			return res;
		}

		public void removeUnreach ()
		{
			List<T> unreach = new List<T> ();
			foreach (T s in states) {
				if (!isReachable (s)) {
					unreach.Add (s);
				}
			}

			foreach (T s in unreach) {
				states.Remove (s);
				finals.Remove (s);

				foreach (var item in trans.Where(x => x.Value.Equals(s)).ToList()) {
					trans.Remove (item.Key);
				}
				foreach (var item in trans.Where(x => x.Key.Item1.Equals(s)).ToList()) {
					trans.Remove (item.Key);
				}
				//finals.Remove (s);
			}
		}

		private bool checkPair (T x, T y) {
			List<Tuple<T,T>> succXY = new List<Tuple<T,T>> (); 
			bool res = false;

			foreach (char a in sigma) {
				if (trans.ContainsKey (Tuple.Create (x, a))) {
					if (trans.ContainsKey (Tuple.Create (y, a)))
						succXY.Add (Tuple.Create(trans [Tuple.Create (x, a)], trans [Tuple.Create (y, a)]));
					else
						throw new Exception ("Error: can not minimize the FSM");
				}
				else
					throw new Exception ("Error: can not minimize the FSM");
			}

			foreach (Tuple<T,T> succ in succXY) {
				foreach (Tuple<Tuple<T,T>,bool> pair in statePairs) {
					if (pair.Item1.Item1.Equals (succ.Item1) && pair.Item1.Item2.Equals (succ.Item2)
						|| pair.Item1.Item1.Equals (succ.Item2) && pair.Item1.Item2.Equals (succ.Item1))
						res = res || pair.Item2;
				}	
			}

			return res;
		}

		private void markPairs () {
			List<Tuple<Tuple<T,T>,bool>> oldPairs = new List<Tuple<Tuple<T, T>, bool>> (statePairs);
			for (int i = 0; i < statePairs.Count; i++) {
				if (statePairs.ElementAt (i).Item2 == false) {
					if (checkPair (statePairs.ElementAt (i).Item1.Item1, statePairs.ElementAt (i).Item1.Item2) == true) {
						statePairs.Add (Tuple.Create (Tuple.Create (statePairs [i].Item1.Item1, statePairs [i].Item1.Item2), true));
						statePairs.RemoveAt (i);
					}
				}
			}
			if (!(new HashSet<Tuple<Tuple<T, T>,bool>>(statePairs).SetEquals((oldPairs))))
				markPairs ();

		}
			

		public void minimize () {
			List<List<T>> toMerge = new List<List<T>> ();
			List<T> tmpList;
			Dictionary<T,T> mergeMap = new Dictionary<T, T> ();
			List<Tuple<T,char,T>> transList = new List<Tuple<T, char, T>> ();
			bool inserted = false;
			T k;

			markPairs ();

			foreach (Tuple<Tuple<T,T>,bool> pair in statePairs.Where(x => x.Item2 == false)) {
				
				if (toMerge.Count == 0) {
					tmpList = new List<T>{ pair.Item1.Item1, pair.Item1.Item2 };
					toMerge.Add (tmpList);
				} else {
					inserted = false;
					for (int i = 0; i < toMerge.Count; i++) {
						if (toMerge [i].Contains (pair.Item1.Item1) && !toMerge [i].Contains (pair.Item1.Item2)) {
							toMerge [i].Add (pair.Item1.Item2);
							inserted = true;
						} else if (toMerge [i].Contains (pair.Item1.Item2) && !toMerge [i].Contains (pair.Item1.Item1)) {
							toMerge [i].Add (pair.Item1.Item1);
							inserted = true;
						} else if (toMerge [i].Contains (pair.Item1.Item1) && toMerge [i].Contains (pair.Item1.Item2)) {
							inserted = true;
						}
					}
					if (inserted == false) {
						tmpList = new List<T>{pair.Item1.Item1, pair.Item1.Item2};
						toMerge.Add (tmpList);					
					}
				}
			}

			foreach (List<T> m in toMerge) {
				k = m [0];
				for (int i = 1; i < m.Count; i++) {
					mergeMap.Add (m[i], k);
				}
			}

			for (int i = 0; i < states.Count; i++) {
				if (mergeMap.ContainsKey (states [i]))
					states [i] = mergeMap [states [i]];
			}

			states = states.Distinct ().ToList ();

			if (mergeMap.ContainsKey (initial))
				initial = mergeMap [initial];

			for (int i = 0; i < finals.Count; i++) {
				if (mergeMap.ContainsKey (finals [i]))
					finals [i] = mergeMap [finals [i]];
			}

			finals = finals.Distinct ().ToList ();

			T tmpOrigin, tmpDest;
			foreach (KeyValuePair<Tuple<T,char>,T> kv in trans) {
				if (mergeMap.ContainsKey (kv.Key.Item1))
					tmpOrigin = mergeMap [kv.Key.Item1];
				else
					tmpOrigin = kv.Key.Item1;
				if (mergeMap.ContainsKey (kv.Value))
					tmpDest = mergeMap [kv.Value];
				else
					tmpDest = kv.Value;
				transList.Add (Tuple.Create (tmpOrigin,kv.Key.Item2,tmpDest));
			}

			transList = transList.Distinct ().ToList ();

			trans = new Dictionary<Tuple<T, char>, T> ();

			foreach (Tuple<T,char,T> t in transList) {
				trans.Add(Tuple.Create(t.Item1,t.Item2),t.Item3);
			}

		}
			
	}

	public class ProductFSM<T>
	{
		public List<Tuple<T,T>> states = new List<Tuple<T,T>>();
		public List<char> sigma;
		public Dictionary<Tuple<Tuple<T,T>,char>,Tuple<T,T>> trans = new Dictionary<Tuple<Tuple<T, T>, char>, Tuple<T, T>> ();
		public Tuple<T,T> initial;
		public List<Tuple<T,T>> finals = new List<Tuple<T, T>> ();

		private Dictionary<Tuple<T,T>, int> stateMap = new Dictionary<Tuple<T,T>, int> ();


		public ProductFSM (FSM<T> m1, FSM<T> m2) 
		{
			if (!((m1.sigma.Count == m2.sigma.Count) && !m1.sigma.Except (m2.sigma).Any ())) {
				throw new Exception ("Different alphabets");
			}
			foreach (T s1 in m1.states) {
				foreach (T s2 in m2.states) {
					this.states.Add(new Tuple<T,T>(s1,s2));
				}
			}
			this.sigma = m1.sigma;

			foreach (T i in m1.states) {
				foreach (T j in m1.states) {
					foreach (T p in m2.states) {
						foreach (T q in m2.states) {
							foreach (char a in m1.sigma) {
								if (m1.trans.ContainsKey(Tuple.Create(i,a)) && m2.trans.ContainsKey(Tuple.Create(p,a))) {
									if (m1.trans [Tuple.Create(i,a)].Equals(j) && m2.trans [Tuple.Create(p,a)].Equals(q)) {
										this.trans.Add (Tuple.Create (Tuple.Create (i, p), a), Tuple.Create (j, q));
									}
								}								
							}
						}
					}
				}
			}


			this.initial = Tuple.Create (m1.initial, m2.initial);

			foreach (Tuple<T,T> s in this.states) {
				if (m1.finals.Exists (x => x.Equals(s.Item1)) || m2.finals.Exists (y => y.Equals(s.Item2))) {
					this.finals.Add(Tuple.Create(s.Item1,s.Item2));
				}
			}

			int z = 0;

			foreach (Tuple<T,T> x in this.states) {
				stateMap.Add (x, z);
				z++;
			}

		}

		public void Print ()
		{
			Console.WriteLine ("states: ");
			foreach (Tuple<T,T> s in states) {
				Console.Write ("{0} ", s);
			}
			Console.WriteLine ("\nalphabet:");
			foreach (char a in sigma) {
				Console.Write ("{0} ", a);
			}
			Console.WriteLine ("\ntransitions:");
			foreach (KeyValuePair<Tuple<Tuple<T,T>,char>,Tuple<T,T>> t in trans) {
				Console.WriteLine ("{0} -{1}-> {2}", t.Key.Item1, t.Key.Item2, t.Value);
			}
			Console.WriteLine ("initial: \n{0}", initial);
			Console.WriteLine ("finals: ");
			foreach (Tuple<T,T> f in finals) {
				Console.Write ("{0} ", f);
			}
			Console.WriteLine ("");
		}

		public FSM<int> Integerize ()
		{
			List<int> statesi = new List<int> ();
			foreach (Tuple<T,T> s in this.states) {
				if (stateMap.ContainsKey (s))
					statesi.Add (stateMap [s]);
				else
					throw new Exception ("Can not integerize the FSM");
			}

			Dictionary<Tuple<int,char>,int> transi = new Dictionary<Tuple<int, char>, int> ();
			foreach (KeyValuePair<Tuple<Tuple<T,T>,char>,Tuple<T,T>> t in this.trans) {
				if (stateMap.ContainsKey (t.Key.Item1) && stateMap.ContainsKey (t.Value))
					transi.Add (Tuple.Create (stateMap [t.Key.Item1], t.Key.Item2), stateMap [t.Value]);
				else
					throw new Exception ("Can not integerize the FSM");
			}

			int initi = 0;
			if (stateMap.ContainsKey (this.initial))
				initi = stateMap [this.initial];
			else
				throw new Exception ("Can not integerize the FSM");

			List<int> fini = new List<int> ();
			foreach (Tuple<T,T> f in this.finals) {
				if (stateMap.ContainsKey (f))
					fini.Add (stateMap [f]);
				else
					throw new Exception ("Can not integerize the FSM");
			}

			return new FSM<int> (statesi, this.sigma, transi, initi, fini);
		}

	}
		
}

