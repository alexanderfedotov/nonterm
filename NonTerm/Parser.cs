﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace NonTerm
{
	public class Parser
	{

		public List<Tuple<string,string>> rules;

		private List<Tuple<string,string>> ProblemParser(XmlReader reader)
		{
			Tuple<List<Tuple<string,string>>, XmlReader> result;
			if (reader.Read() && reader.NodeType == XmlNodeType.Element && reader.Name == "trs")
			{
				result = TRSParser(reader);
				reader = result.Item2;
				if (reader.Read() && reader.NodeType == XmlNodeType.Element && reader.Name == "strategy")
				{
					while (reader.Read() && !(reader.NodeType == XmlNodeType.EndElement && reader.Name == "metainformation")) { }
				}
				if (!reader.Read() || (reader.NodeType != XmlNodeType.EndElement || reader.Name != "problem"))
				{
					throw new ApplicationException("Failed to parse \"problem\" tag");
				}
				else return result.Item1;
			}
			else
			{
				throw new ApplicationException("Failed to parse \"problem\" tag");
			}
		}

		private Tuple<List<Tuple<string,string>>, XmlReader> TRSParser(XmlReader reader)
		{
			Tuple<List<Tuple<string,string>>, XmlReader> result;
			if (reader.Read() && reader.NodeType == XmlNodeType.Element && reader.Name == "rules")
			{
				result = RulesParser(reader);
				reader = result.Item2;
				if (reader.Read() && reader.NodeType == XmlNodeType.Element && reader.Name == "signature")
				{
					while (reader.Read() && reader.Name != "signature") { }
				}
				if ((reader.Read() && reader.NodeType == XmlNodeType.EndElement && reader.Name != "trs"))
				{
					throw new ApplicationException("Failed to parse \"trs\" tag");
				}
				else return result;
			}
			else
			{
				throw new ApplicationException("Failed to parse \"trs\" tag");
			}
		}

		private Tuple<List<Tuple<string,string>>, XmlReader> RulesParser(XmlReader reader)
		{
			Tuple<Tuple<string,string>, XmlReader> result;
			List<Tuple<string,string>> rules = new List<Tuple<string,string>>();
			while(reader.Read() && reader.Name != "rules")
			{
				if (reader.NodeType != XmlNodeType.EndElement && reader.Name == "rule")
				{
					result = RuleParser(reader);
					rules.Add(result.Item1);
					reader = result.Item2;
				}
			}
			if (reader.Name != "rules") throw new ApplicationException("Failed to parse \"rules\" tag");
			else return new Tuple<List<Tuple<string,string>>, XmlReader>(rules, reader);
		}

		private Tuple<Tuple<string,string>, XmlReader> RuleParser(XmlReader reader)
		{
			Tuple<string, XmlReader> lhsResult, rhsResult;
			if (reader.Read() && reader.NodeType == XmlNodeType.Element && reader.Name == "lhs")
			{
				lhsResult = LhsParser(reader);
				reader = lhsResult.Item2;
			}
			else
			{
				throw new ApplicationException("Failed to parse \"rule\" tag");
			}
			if (reader.Read() && reader.NodeType == XmlNodeType.Element && reader.Name == "rhs")
			{
				rhsResult = RhsParser(reader);
				reader = rhsResult.Item2;
			}
			else
			{
				throw new ApplicationException("Failed to parse \"rule\" tag");
			}
			if (!reader.Read() || reader.NodeType != XmlNodeType.EndElement || reader.Name != "rule")
			{
				throw new ApplicationException("Failed to parse \"rule\" tag");
			}
			return new Tuple<Tuple<string,string>, XmlReader>(Tuple.Create(lhsResult.Item1, rhsResult.Item1), reader);
		}

		private Tuple<string, XmlReader> LhsParser(XmlReader reader)
		{
			Tuple<string, XmlReader> result;

			if (reader.Read())
			{
				if (reader.NodeType == XmlNodeType.Element && reader.Name == "funapp") result = FunAppParser(reader);
				else if (reader.NodeType == XmlNodeType.Element && reader.Name == "var")
				{
					reader.Read();
					reader.Read();
					result = new Tuple<string, XmlReader>("", reader);
				}
				else throw new ApplicationException("Failed to parse \"lhs\" tag");
			}
			else throw new ApplicationException("Failed to parse \"lhs\" tag");

			reader = result.Item2;
			if (!reader.Read() || reader.NodeType != XmlNodeType.EndElement || reader.Name != "lhs") throw new ApplicationException("Failed to parse \"lhs\" tag");
			else return new Tuple<string, XmlReader>(result.Item1, reader); 
		}

		private Tuple<string, XmlReader> RhsParser(XmlReader reader)
		{
			Tuple<string, XmlReader> result;

			if (reader.Read())
			{
				if (reader.NodeType == XmlNodeType.Element && reader.Name == "funapp") result = FunAppParser(reader);
				else if (reader.NodeType == XmlNodeType.Element && reader.Name == "var")
				{
					reader.Read();
					reader.Read();
					result = new Tuple<string, XmlReader>("", reader);
				}
				else throw new ApplicationException("Failed to parse \"rhs\" tag");
			}
			else throw new ApplicationException("Failed to parse \"rhs\" tag");

			reader = result.Item2;
			if (!reader.Read() || reader.NodeType != XmlNodeType.EndElement || reader.Name != "rhs") throw new ApplicationException("Failed to parse \"rhs\" tag");
			else return new Tuple<string, XmlReader>(result.Item1, reader); 
		}

		private Tuple<string, XmlReader> NameParser(XmlReader reader)
		{
			string result;
			if (reader.NodeType == XmlNodeType.Element && reader.Name == "name")
			{
				reader.Read();
				result = reader.Value;
			}
			else throw new ApplicationException("Failed to parse \"name\" tag");
			if (!reader.Read() || reader.NodeType != XmlNodeType.EndElement || reader.Name != "name") throw new ApplicationException("Failed to parse \"name\" tag"); 
			else return new Tuple<string, XmlReader>(result, reader);
		}

		private Tuple<string, XmlReader> FunAppParser(XmlReader reader)
		{
			Tuple<string, XmlReader> nameResult;
			Tuple<string, XmlReader> argResult;
			if (reader.Read() && reader.NodeType == XmlNodeType.Element && reader.Name == "name") nameResult = NameParser(reader);
			else throw new ApplicationException("Failed to parse \"funapp\" tag");
			reader = nameResult.Item2;
			if (reader.Read() && reader.NodeType == XmlNodeType.Element && reader.Name == "arg") argResult = ArgParser(reader);
			else throw new ApplicationException("Failed to parse \"funapp\" tag");
			if (!reader.Read() || reader.NodeType != XmlNodeType.EndElement || reader.Name != "funapp") throw new ApplicationException("Failed to parse \"funapp\" tag");
			else return new Tuple<string, XmlReader>(nameResult.Item1 + argResult.Item1, reader); 
		}

		private Tuple<string, XmlReader> ArgParser(XmlReader reader)
		{
			Tuple<string, XmlReader> funResult = new Tuple<string, XmlReader>("", reader);
			if (reader.Read() && reader.NodeType == XmlNodeType.Element && reader.Name == "var")
			{
				reader.Read();
				if (!reader.Read() || reader.NodeType != XmlNodeType.EndElement || reader.Name != "var") throw new ApplicationException("Failed to parse \"arg\" tag");
			}
			if (reader.NodeType == XmlNodeType.Element && reader.Name == "funapp")
			{
				funResult = FunAppParser(reader);
				reader = funResult.Item2;
			}
			if (!reader.Read() || reader.NodeType != XmlNodeType.EndElement || reader.Name != "arg") throw new ApplicationException("Failed to parse \"arg\" tag");
			else return new Tuple<string, XmlReader>(funResult.Item1, reader);
		}

		public Parser (string inpPath)
		{
			XmlReaderSettings settings = new XmlReaderSettings();
			settings.IgnoreComments = true;
			settings.IgnoreProcessingInstructions = true;
			settings.IgnoreWhitespace = true;

			XmlReader reader = XmlReader.Create (inpPath, settings);



			while (reader.Read() && reader.NodeType != XmlNodeType.EndElement && reader.Name != "problem")
			{
			}
			if (reader.EOF || reader.NodeType != XmlNodeType.Element || reader.Name != "problem")
			{
				throw new ApplicationException("Parsing failed");
			}
			else
			{
				rules = ProblemParser(reader);
			}

		}
	}
}

